### How to make "nano" code editor to be named as "rossa" (plus with HySS syntax highlighting)

I am a big fan of [nano code editor](https://www.nano-editor.org/), and I want to make **nano** from source code and rename it to **rossa** with just **sed** command line. Also, I want to enable syntax highlighting support for HySS script files.

The way to do that is fairly easy, with a few tricks using **sed** command line and using your current **nano** code editor or **vi**.

You need to get started this way from nano source code at [https://www.nano-editor.org/](https://www.nano-editor.org/)

This assumes you already have a built-in **nano**/**vi** editor or any editor that is included with other packages such as **build-essential**.

The required packages you need to install to build and install nano from the source code are : **build-essential**, **libncursesw5-dev**, and **groff**.

The steps are as follows :

	$ wget https://www.nano-editor.org/dist/v5/nano-5.0.tar.gz
	$ tar xzvf nano-5.0.tar.gz
	$ cd nano-5.0
	$ sed -i 's/rnano/rossa/g' src/Makefile.in
	$ sed -i '/html.nanorc/ a hyss.nanorc \\' syntax/Makefile.in

Create a file **syntax/hyss.nanorc** using your current nano editor :

	$ nano syntax/hyss.nanorc

Copy and paste content of file [hyss.nanorc](https://bitbucket.org/ranrossa/hyss-nanorc/src/master/hyss.nanorc) to its content. Save it (press **ctr+x** and then **Y** and press **Enter**).

Then run :

	$ ./configure
	$ make
	$ sudo make install

#### Post installation step

Ceck the list of files in **/usr/local/share/nano** :

	$ cd /usr/local/share/nano
	$ ls

The **hyss.nanorc** file should be there.

Create a file in your home directory **~/.nanorc** with your new **rossa** code editor :

	$ rossa ~/.nanorc

Add a line of code below to its content :

	include "/usr/local/share/nano/hyss.nanorc"

Save it (press **ctrl+x**, and **Y** and then press **Enter**)

Test it :

	$ rossa hello.hyss

etc. Screenshots can be found [here](https://ranrossa.blogspot.com/2020/08/how-to-make-nano-code-editor-to-be-named-as-rossa-plus-with-hyss-syntax-highlighting.html).
